# -*- coding: utf-8 -*-
"""
Created on Wed Mar  8 17:57:05 2017

@author: dani
"""

import unittest
import fibonacci
class fibonaccitest(unittest.TestCase):
 def setUp(self):
     print("setUp: INICIANDO TEST")
 # Instanciamos de la clase Triangulo
     self.fibonacci = fibonacci.Fibonacci()
      # Se ejecuta despues de cada test
 def tearDown(self):
     print("tearDown: FINALIZANDO TEST")
 def test1_Of_5_Month_3_pair(self):
        print("Test1")
        self.fibonacci.month=5
        self.fibonacci.pair=3
        resultado=self.fibonacci.compute(self.fibonacci.month,self.fibonacci.pair)
        self.assertEqual(resultado,19)
 def test_2_Of_0_Month_0_pair(self):
         print("Test2")
         self.fibonacci.month=0
         self.fibonacci.pair=0
         resultado=self.fibonacci.compute(self.fibonacci.month,self.fibonacci.pair)
         self.assertEqual(resultado,0)
 def test3_Of_1_Month_0_pair(self):
         print("Test3")
         self.fibonacci.month=1
         self.fibonacci.pair=0
         resultado=self.fibonacci.compute(self.fibonacci.month,self.fibonacci.pair)
         self.assertEqual(resultado,1)
         
 def test4__Of_41_Month_0_pair_must_give_an_exception(self):
         print("Test4")
         with self.assertRaises(Exception):
            self.fibonacci(41,3)
 def test5__Of_4_Month_10_pair_must_give_an_exception(self):
         print("Test5")
         with self.assertRaises(Exception):
            self.fibonacci(4,10)
 def test6__Of_negative_month(self):
         print("Test5")
         with self.assertRaises(Exception):
            self.fibonacci(-4,10)
          
 def test6__Of_negative_pair(self):
         print("Test5")
         with self.assertRaises(Exception):
            self.fibonacci(4,-1)   
        
if __name__=="__main__":
        unittest.main()
     
