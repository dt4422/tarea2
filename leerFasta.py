# -*- coding: utf-8 -*-
"""
Created on Fri Mar 10 12:06:56 2017

@author: dani
"""

def fasta(fichero):
    identificador=''
    secuencia=''
    
    palabras=[]
    for line in open(fichero): #leemos fichero
        if line[0]=='>':
           
            if identificador!='': 
                
                    palabras.append((identificador,secuencia)) #añadimos a nuestra lista un nuevo valor
          
           
            line=line.split("\n") #dividimos toda la nueva linea que va a ser nuestra cabecera
            identificador=line[0].rstrip() # una vez que esta divida la almacenamos dentro del indentificador
            secuencia=''#es necesario ya que si no el proximo elemento de la lista tendra nucleotidos de la anterior
                
        else:
                    secuencia=secuencia+line.rstrip() # concatenamos las lineas
    palabras.append((identificador,secuencia))               
    return palabras #devolvemos la lista
    
               
  
            
           